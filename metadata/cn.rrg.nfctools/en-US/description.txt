At present, Proxmark 3 RDV4.0 can be connected via Bluetooth or USB to read, write and detect common Tags.
It is a necessary research tool for RFID enthusiasts.
Later, it will be compatible with more hardware devices and related firmware.
 
